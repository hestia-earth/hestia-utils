# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.13.21](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.13.20...v0.13.21) (2025-02-11)


### Bug Fixes

* **date:** fix `monthsBefore` function ([42e4400](https://gitlab.com/hestia-earth/hestia-utils/commit/42e44007cbcf8da1946de75d20e9ea6f140926ea))

### [0.13.20](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.13.19...v0.13.20) (2025-02-11)


### Features

* **date:** add `monthsBefore` method ([060340a](https://gitlab.com/hestia-earth/hestia-utils/commit/060340a86393961fed17d6db14e91645998599ee))

### [0.13.19](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.13.18...v0.13.19) (2025-01-31)


### Features

* **python-pipeline:** add `get_stage` function ([9130f13](https://gitlab.com/hestia-earth/hestia-utils/commit/9130f13f735a9fdcedf47511820d21a961cd2fa4))


### Bug Fixes

* **pipeline:** handle no related nodes found ([703b2b6](https://gitlab.com/hestia-earth/hestia-utils/commit/703b2b6ae9ddee5ccf9a14427de477fde23e1ae9))

### [0.13.18](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.13.17...v0.13.18) (2025-01-29)


### Features

* **pipeline:** add `load_cache` method ([63e26bc](https://gitlab.com/hestia-earth/hestia-utils/commit/63e26bcb2721fed8fd7afe8d72f4b4a7bfe1cf5b))

### [0.13.17](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.13.16...v0.13.17) (2025-01-22)


### Features

* **python emission:** extend to other termType ([4fbde64](https://gitlab.com/hestia-earth/hestia-utils/commit/4fbde64010532b683371e89d32451cf3a44f3799))

### [0.13.16](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.13.15...v0.13.16) (2025-01-22)


### Features

* **python tools:** add `is_boolean` method ([e8dd920](https://gitlab.com/hestia-earth/hestia-utils/commit/e8dd92059eecf45dfe2d657b751aa19ed7467dd0))

### [0.13.15](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.13.14...v0.13.15) (2025-01-08)


### Bug Fixes

* **javascript boolean:** handle no value should be false ([74b971f](https://gitlab.com/hestia-earth/hestia-utils/commit/74b971f56d8609f48bbdf5b051b490c8dce40d6c))

### [0.13.14](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.13.13...v0.13.14) (2025-01-08)


### Features

* **pipeline:** only fetch from related API if key not found in cache ([ed23cfc](https://gitlab.com/hestia-earth/hestia-utils/commit/ed23cfccd67eab0af3c197afb0229a3ae80ac7ba))

### [0.13.13](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.13.12...v0.13.13) (2025-01-07)


### Features

* **python:** log message get related using API ([743113d](https://gitlab.com/hestia-earth/hestia-utils/commit/743113d4f38a991d3ac57460368b4eaeeb6ca3ea))

### [0.13.12](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.13.11...v0.13.12) (2024-12-20)


### Bug Fixes

* **emission:** handle no primary product check in system boundary ([eb43537](https://gitlab.com/hestia-earth/hestia-utils/commit/eb4353758dfca710852aa16fcdd92bf15135dddf))

### [0.13.11](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.13.10...v0.13.11) (2024-12-04)


### Features

* **python:** add emission utils to list emissions in system boundary ([bdcd095](https://gitlab.com/hestia-earth/hestia-utils/commit/bdcd09555eb0725dca7a3d36c93639058a420f7e))

### [0.13.10](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.13.9...v0.13.10) (2024-11-26)


### Bug Fixes

* **pyton pipeline:** return unique list of related nodes ([d84df99](https://gitlab.com/hestia-earth/hestia-utils/commit/d84df997f6e4ed796cc2d83c23a9c5d3313e55a3))

### [0.13.9](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.13.7...v0.13.9) (2024-11-18)


### Features

* **python:** add `to_precision` method ([4fc3cf3](https://gitlab.com/hestia-earth/hestia-utils/commit/4fc3cf3df97ddf75efe54ffda82f0be3a65edb3f))


### Bug Fixes

* **pipeline:** fix argument error on read metadata ([d831c92](https://gitlab.com/hestia-earth/hestia-utils/commit/d831c92e1e0819c1bc3bf4bba75b0a6eb3a37dc1))
* **python pipeline:** fix error reading data on wrong nodes ([9914e4a](https://gitlab.com/hestia-earth/hestia-utils/commit/9914e4a48d858b32caf577f2670a3bd13d44435d))

### [0.13.8](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.13.7...v0.13.8) (2024-11-18)


### Bug Fixes

* **pipeline:** fix argument error on read metadata ([53cee39](https://gitlab.com/hestia-earth/hestia-utils/commit/53cee39a7a6af74598bc0f3cc50f6d3b96578015))

### [0.13.7](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.13.6...v0.13.7) (2024-11-16)


### Features

* **python pipeline:** add `get_related_nodes` function ([f673dea](https://gitlab.com/hestia-earth/hestia-utils/commit/f673dea24719f5ad7252a614943c1f7e5471c5cd))

### [0.13.6](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.13.5...v0.13.6) (2024-11-15)


### Features

* **python pipeline:** add function to get related nodes with data ([b7987be](https://gitlab.com/hestia-earth/hestia-utils/commit/b7987be838878115d44096c6c812e705b6d9f58f))

### [0.13.5](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.13.4...v0.13.5) (2024-10-21)


### Bug Fixes

* **lookup:** handle `/` chars in lookup column name ([0dbc1e4](https://gitlab.com/hestia-earth/hestia-utils/commit/0dbc1e4a7ffe73758600e2b167052fd5ac58cb93))

### [0.13.4](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.13.3...v0.13.4) (2024-09-04)


### Features

* **python date:** add `is_in_months` function ([6976851](https://gitlab.com/hestia-earth/hestia-utils/commit/6976851ed1bc85ed82b715dd226f56369019243d))

### [0.13.3](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.13.2...v0.13.3) (2024-08-23)


### Features

* **model:** handle extra match params in `find_term_match` ([0698746](https://gitlab.com/hestia-earth/hestia-utils/commit/06987463ae4bd3e92b82566937b949f8d242d6db))

### [0.13.2](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.13.1...v0.13.2) (2024-07-29)


### Bug Fixes

* **blank node:** fix default value not used ([f2b18a7](https://gitlab.com/hestia-earth/hestia-utils/commit/f2b18a75650410129920a8ec015213a7ccf35fd3))

### [0.13.1](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.13.0...v0.13.1) (2024-07-29)


### Features

* **blank node:** add function to retrieve value using `arrayTreatment` ([62cdf65](https://gitlab.com/hestia-earth/hestia-utils/commit/62cdf65ae4a4957cd4a6730beefbd80be058e176))

## [0.13.0](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.12.7...v0.13.0) (2024-06-21)


### ⚠ BREAKING CHANGES

* **pivot-csv:** internal `_pivot_nodes` function renamed `pivot_nodes`.

### Bug Fixes

* **pivot-csv:** export function `pivot_nodes` ([5e51ac7](https://gitlab.com/hestia-earth/hestia-utils/commit/5e51ac71cba4b297c8e0f7af77b73eb1031727b4))

### [0.12.7](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.12.6...v0.12.7) (2024-06-19)


### Bug Fixes

* **string:** handle all dates in `toDashCase` ([7d6c6a1](https://gitlab.com/hestia-earth/hestia-utils/commit/7d6c6a1b2ae34ba13f4eed0c11ef5a53838cd24c))

### [0.12.6](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.12.5...v0.12.6) (2024-06-17)


### Bug Fixes

* **requirements:** use numpy version 1 ([fa59f23](https://gitlab.com/hestia-earth/hestia-utils/commit/fa59f236f478b0553095d52f4ead1a9bf7306724))

### [0.12.5](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.12.4...v0.12.5) (2024-05-16)


### Features

* **term:** handle boolean values in `propertyValue` ([02e300d](https://gitlab.com/hestia-earth/hestia-utils/commit/02e300dc7889ffd010c7e7763c45a5a1882a7b02))


### Bug Fixes

* **pivot csv:** fix error with empty rows ([334f4f7](https://gitlab.com/hestia-earth/hestia-utils/commit/334f4f72024d2d137f584f1380ced06d77c10fcc))

### [0.12.4](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.12.3...v0.12.4) (2024-04-08)


### Features

* **python utils:** handle `str` term types in `filter_list_term_type` ([28ce65b](https://gitlab.com/hestia-earth/hestia-utils/commit/28ce65bf7795740f02ade415a1aa3907b33fab20))

### [0.12.3](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.12.2...v0.12.3) (2024-02-26)


### Features

* **pivot json:** ignore `added` and `updated` arrays ([64ad88e](https://gitlab.com/hestia-earth/hestia-utils/commit/64ad88ed9ec849a08acaa6f2b9e95a4eed611068))

### [0.12.2](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.12.1...v0.12.2) (2024-02-21)


### Bug Fixes

* **python tools:** handle list average/sum of empty values ([cc213b2](https://gitlab.com/hestia-earth/hestia-utils/commit/cc213b2b9ba853da8340de024f55045b09134fdf))

### [0.12.1](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.12.0...v0.12.1) (2023-12-11)


### Bug Fixes

* **python pivot:** add missing init file ([7214f1c](https://gitlab.com/hestia-earth/hestia-utils/commit/7214f1c0ed66977b3bb70118de236b5c1ee552b6))

## [0.12.0](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.11.13...v0.12.0) (2023-12-11)


### ⚠ BREAKING CHANGES

* **python table:** Pivoting functions moved to `hestia_earth.utils.pivot`.

### Features

* **python pivot:** add fast func for pivoting json ([5e16d46](https://gitlab.com/hestia-earth/hestia-utils/commit/5e16d463d55436850c4b217174b75ce10340e628))
* **python pivot:** handle properties.term.@id/value uniqueness fields as special case ([09d1216](https://gitlab.com/hestia-earth/hestia-utils/commit/09d12167ba299294ab37f0c47dbcedd0b40201a1))


* **python table:** extract pivoting code to own module ([086cf75](https://gitlab.com/hestia-earth/hestia-utils/commit/086cf7590520f4fd0bfd17cf50092609cc14fd4d))

### [0.11.13](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.11.12...v0.11.13) (2023-12-04)


### Features

* **python pipeline:** add function to parse AWS events ([0aaa639](https://gitlab.com/hestia-earth/hestia-utils/commit/0aaa639e241f609385d3a62f1314d3ea879c05c7))

### [0.11.12](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.11.11...v0.11.12) (2023-11-07)


### Bug Fixes

* **pipeline:** fix numpy JSON encoding issue ([91c3720](https://gitlab.com/hestia-earth/hestia-utils/commit/91c3720147e8dae70617c3fd08841cc1c62066f5))

### [0.11.11](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.11.10...v0.11.11) (2023-11-06)


### Bug Fixes

* **pipeline:** upload json with correct `contentType` ([8ef04b7](https://gitlab.com/hestia-earth/hestia-utils/commit/8ef04b7b54c5ad23275754e09409b41de21f53d0))

### [0.11.10](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.11.9...v0.11.10) (2023-11-06)


### Bug Fixes

* **storage:** add missing region on SNS client ([e6beb54](https://gitlab.com/hestia-earth/hestia-utils/commit/e6beb54d2c74b80bd72c3fc5875b3f4e9381580e))

### [0.11.9](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.11.8...v0.11.9) (2023-11-06)


### Bug Fixes

* **pipeline:** add missing `warning` section ([eeb87c9](https://gitlab.com/hestia-earth/hestia-utils/commit/eeb87c9d8ec80010bb05043c804c6f20d0683085))

### [0.11.8](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.11.7...v0.11.8) (2023-11-06)


### Bug Fixes

* **python table:** do not pivot emissions.transport/inputs ([afea3ac](https://gitlab.com/hestia-earth/hestia-utils/commit/afea3ac4ddcb7edc73c847a86e5b115fad52d361))

### [0.11.7](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.11.6...v0.11.7) (2023-10-20)


### Features

* **python:** add s3 function to read file size ([7147f9d](https://gitlab.com/hestia-earth/hestia-utils/commit/7147f9d54886337cfe9627c5df3e01796db13f88))
* **python:** add utils for pipeline ([36d7583](https://gitlab.com/hestia-earth/hestia-utils/commit/36d758300939f9495459dd26dd1689af9a42cb04))

### [0.11.6](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.11.5...v0.11.6) (2023-10-20)

### [0.11.5](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.11.4...v0.11.5) (2023-10-11)


### Features

* **python table:** add function to pivot list of nodes ([bbc939a](https://gitlab.com/hestia-earth/hestia-utils/commit/bbc939a60da2f628256c3435eef0f8f1fd3d97a6)), closes [#28](https://gitlab.com/hestia-earth/hestia-utils/issues/28)

### [0.11.4](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.11.3...v0.11.4) (2023-09-14)


### Bug Fixes

* **lookup:** handle parenthesis in lookup column name ([649b2cb](https://gitlab.com/hestia-earth/hestia-utils/commit/649b2cbafdafce68e9fee41b9f0d95b9f0b14e69))

### [0.11.3](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.11.2...v0.11.3) (2023-06-27)


### Bug Fixes

* **javascript:** fix `isNumber` not working on single digits ([229e260](https://gitlab.com/hestia-earth/hestia-utils/commit/229e260d51f2281bfa60fff8542b5eca0af06aa4))
* **javascript string:** fix dash case for numbers ([9386fd9](https://gitlab.com/hestia-earth/hestia-utils/commit/9386fd94b2d8bdc8280582b71e336a5b1108d3cd))

### [0.11.2](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.11.1...v0.11.2) (2023-04-27)


### Features

* **javascript:** add `extension` arg for validate terms ([2ba0997](https://gitlab.com/hestia-earth/hestia-utils/commit/2ba09978a3b30c57d0e4270ba7ff0b0ca3a9b2f1))
* **javascript:** handle null values when checking undefined ([8a5f2cc](https://gitlab.com/hestia-earth/hestia-utils/commit/8a5f2ccd110380a7faaf563d0a15ba62a4387dc1))
* **python table:** add deep pivoting ([9c2aada](https://gitlab.com/hestia-earth/hestia-utils/commit/9c2aadaf185b72c0873ef54b55cc77e153791a4f))

### [0.11.1](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.11.0...v0.11.1) (2023-03-16)


### Bug Fixes

* **python lookup:** remove warnings on empty files ([0f62b7b](https://gitlab.com/hestia-earth/hestia-utils/commit/0f62b7b53cab7b23c404ec651785e9f71f1c2ca4))

## [0.11.0](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.32...v0.11.0) (2023-03-13)


### ⚠ BREAKING CHANGES

* **python table:** schema min version `17` required

### Features

* **python model:** filter list `termType` by multiple values ([fa3d8ce](https://gitlab.com/hestia-earth/hestia-utils/commit/fa3d8ce4660476c46d2bdc29d1949724e913ea58))
* **python table:** handle any uniqueness field set (and refactor) ([214b7b3](https://gitlab.com/hestia-earth/hestia-utils/commit/214b7b30876ccf7959f6ab6c7a0e8ef2f36ded87))
* **python table:** various improvements ([84ef884](https://gitlab.com/hestia-earth/hestia-utils/commit/84ef884992d42fd30aa46740160210f41b36cdaa))

### [0.10.32](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.31...v0.10.32) (2023-02-23)


### Features

* **python model:** include `name` in linked `Source` ([372725b](https://gitlab.com/hestia-earth/hestia-utils/commit/372725b980a428ff1d7d2364cd5ce7de9b7e8292))

### [0.10.31](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.30...v0.10.31) (2023-02-06)


### Bug Fixes

* **javascript number:** fix floating precision on precision big numbers ([16fca8e](https://gitlab.com/hestia-earth/hestia-utils/commit/16fca8ed267b5ae99771475911145e81c1e68a14)), closes [#22](https://gitlab.com/hestia-earth/hestia-utils/issues/22)

### [0.10.30](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.29...v0.10.30) (2023-01-02)


### Features

* **javascript number:** add `toComma` function ([72eb3be](https://gitlab.com/hestia-earth/hestia-utils/commit/72eb3bebfc7efc9f6b8f7307df4253e3c0d95419))

### [0.10.29](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.28...v0.10.29) (2022-12-26)


### Features

* **validate terms:** reduce number of requests when validating multiple files ([d64a92b](https://gitlab.com/hestia-earth/hestia-utils/commit/d64a92bb11d00519f70eb9c45cb38746af009c4e))

### [0.10.28](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.27...v0.10.28) (2022-12-20)


### Features

* **table:** fetch missing ids from API in `pivot_csv` ([ef09bee](https://gitlab.com/hestia-earth/hestia-utils/commit/ef09bee71c9f84a35934ef8391076455e85bf9e8))


### Bug Fixes

* **python api:** handle find related new format from api `0.17.0` ([25c7595](https://gitlab.com/hestia-earth/hestia-utils/commit/25c7595ed3b4c93834343758a000ee483b33779a))

### [0.10.27](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.26...v0.10.27) (2022-09-20)

### [0.10.26](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.25...v0.10.26) (2022-08-03)


### Bug Fixes

* **javascript number:** handle float in `isNumber` ([1dd5e4f](https://gitlab.com/hestia-earth/hestia-utils/commit/1dd5e4f9212adc00e397a47ff1dc1b04cb0b8f7d))

### [0.10.25](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.24...v0.10.25) (2022-08-03)


### Features

* **javascript string:** handle dash case with underscores ([ea07f71](https://gitlab.com/hestia-earth/hestia-utils/commit/ea07f713b3cb1b1086f5f25d9074234e311836ba))


### Bug Fixes

* **javascript number:** handle dates detection in `isNumber` ([586e212](https://gitlab.com/hestia-earth/hestia-utils/commit/586e212e16fbd5b0c481b895885325643d4e42e5))

### [0.10.24](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.23...v0.10.24) (2022-07-21)


### Bug Fixes

* **term:** array with no values should return property value = `null` ([8cd99a7](https://gitlab.com/hestia-earth/hestia-utils/commit/8cd99a7e632f66dd8755a335c8d00e1fe3c7ca1e))

### [0.10.23](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.22...v0.10.23) (2022-07-18)


### Bug Fixes

* **javascript term:** handle `null` value in property values ([30244e9](https://gitlab.com/hestia-earth/hestia-utils/commit/30244e9d0bf13af699b4f960d643d94e66a93251))
* **python api:** handle file not found in local storage ([17615cd](https://gitlab.com/hestia-earth/hestia-utils/commit/17615cdd0bd9bb69f2d844771de9bb0d9bedeeed))

### [0.10.22](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.21...v0.10.22) (2022-07-11)


### Features

* **date:** add `hoursBefore` and `minutesBefore` functions ([4503e47](https://gitlab.com/hestia-earth/hestia-utils/commit/4503e4714604c78995141d4b2ca169d3372ba654))
* **python tools:** handle `None` values in `list_sum` and `list_average` ([d00368d](https://gitlab.com/hestia-earth/hestia-utils/commit/d00368d69fee2fb5cff5e8e3294dab58dece5c9f))
* **table:** added depth intervals to pivot function, as well as handling shuffled data ([e9de130](https://gitlab.com/hestia-earth/hestia-utils/commit/e9de1305ffefa511f356f6ff901f1eee06522df4))

### [0.10.21](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.20...v0.10.21) (2022-06-28)


### Features

* **javascript date:** add `daysBefore` method ([6649328](https://gitlab.com/hestia-earth/hestia-utils/commit/66493282e3c5f2e394469b5bf7318f5234f05dfe))
* **javascript number:** add molar masses conversions ([0caf171](https://gitlab.com/hestia-earth/hestia-utils/commit/0caf171caa2d5a6a4e651188b1a7eb854959f75c))

### [0.10.20](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.19...v0.10.20) (2022-06-09)


### Features

* **table:** pivot by name instead of id ([a07b774](https://gitlab.com/hestia-earth/hestia-utils/commit/a07b774176478edeffcc16b80c32b1492abb4d6d))


### Bug Fixes

* **javascript number:** fix floating point value in `toPrecision` ([7e16ab7](https://gitlab.com/hestia-earth/hestia-utils/commit/7e16ab79fc09e491ba8b0c0b1a5fa19b7d4a6228))

### [0.10.19](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.18...v0.10.19) (2022-04-22)


### Features

* **javascript string:** improve `keyToLabel` function ([8ae0787](https://gitlab.com/hestia-earth/hestia-utils/commit/8ae07879d9ed4567d6a7d27482f7d2d71668bf6a))

### [0.10.18](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.17...v0.10.18) (2022-04-22)


### Features

* **javascript delta:** add `delta` function ([154c549](https://gitlab.com/hestia-earth/hestia-utils/commit/154c549699d12026ec8faa654a8788b2a340a706))
* **javascript string:** add `toDashCase` function ([034e092](https://gitlab.com/hestia-earth/hestia-utils/commit/034e09296054f6e5581ae4276f856f372a01d42b))
* **javascript term:** add `emptyValue` function ([81fbcd9](https://gitlab.com/hestia-earth/hestia-utils/commit/81fbcd90b508db27de60e340077222244c13466b))
* **javascript terms:** add `propertyValue` function ([9a04e47](https://gitlab.com/hestia-earth/hestia-utils/commit/9a04e47aee4ef063fae9722f1f8daf84f920ede3))

### [0.10.17](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.16...v0.10.17) (2022-04-01)


### Bug Fixes

* **python table:** fix pivoting on impact assessment data ([f20e172](https://gitlab.com/hestia-earth/hestia-utils/commit/f20e1722b9cb6f98f1a844c1f89e48b2853feb91))

### [0.10.16](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.15...v0.10.16) (2022-03-31)


### Bug Fixes

* **table:** handle format nested cycle `[@id](https://gitlab.com/id)` ([ce861dd](https://gitlab.com/hestia-earth/hestia-utils/commit/ce861ddea28db4791827f2e380e2d4bc90d58347))

### [0.10.15](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.14...v0.10.15) (2022-03-31)


### Bug Fixes

* **table:** handle unsafe attempt to use `isnan` ([2759fb2](https://gitlab.com/hestia-earth/hestia-utils/commit/2759fb2da0ac1a02102d7b0c19cb6fbe69a30e5c))

### [0.10.14](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.13...v0.10.14) (2022-03-31)


### Features

* **table:** format remove impacts without a value ([8b391a9](https://gitlab.com/hestia-earth/hestia-utils/commit/8b391a95274fc39a46140332521f50272975fca0))

### [0.10.13](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.12...v0.10.13) (2022-03-31)


### Features

* **python:** add bin `hestia-format-upload` ([bd3737c](https://gitlab.com/hestia-earth/hestia-utils/commit/bd3737cb748209580066de9c73fd9457f1957e76))

### [0.10.12](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.11...v0.10.12) (2022-03-30)


### Bug Fixes

* **python model:** only include keys for linked Term ([4cf7c76](https://gitlab.com/hestia-earth/hestia-utils/commit/4cf7c7638909a484f77651065668eb15b47efde1))

### [0.10.11](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.10...v0.10.11) (2022-03-11)


### Bug Fixes

* **python tools:** handle non-array in `non_empty_list` ([ab2a454](https://gitlab.com/hestia-earth/hestia-utils/commit/ab2a454aba5d759dd65e0eafbfa03db8ecec6879))

### [0.10.10](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.9...v0.10.10) (2022-02-11)


### Bug Fixes

* **python lookup:** do not convert empty values ([4575036](https://gitlab.com/hestia-earth/hestia-utils/commit/4575036ae4d895b19da1e2bb5e8f4b3a7b8c5e76))

### [0.10.9](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.8...v0.10.9) (2022-02-11)


### Bug Fixes

* **python lookup:** handle negative float values ([6c9576e](https://gitlab.com/hestia-earth/hestia-utils/commit/6c9576e1bcf731f4a60327280bb76f9dd0e7b0e4))

### [0.10.8](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.7...v0.10.8) (2022-02-10)


### Bug Fixes

* **storage:** fix load local lookup data as str ([0e1ae31](https://gitlab.com/hestia-earth/hestia-utils/commit/0e1ae3111ec636fd27c6dca2f4ccc8b46503dcd5))

### [0.10.7](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.6...v0.10.7) (2022-02-10)


### Features

* **python storage:** handle local storage of files ([63a81c6](https://gitlab.com/hestia-earth/hestia-utils/commit/63a81c60de95585770a2e91f2e71d5155f857f76))

### [0.10.6](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.5...v0.10.6) (2022-02-10)


### Bug Fixes

* **python lookup:** fix replace missing value in grouped data ([c96886c](https://gitlab.com/hestia-earth/hestia-utils/commit/c96886c7e16a78e24d7109e519a3367192093386))

### [0.10.5](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.4...v0.10.5) (2022-02-10)


### Bug Fixes

* **python lookup:** handle missing values in lookup ([3c01f1f](https://gitlab.com/hestia-earth/hestia-utils/commit/3c01f1f5bd26ab4caf843c4b3574b340f27e26ac))

### [0.10.4](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.3...v0.10.4) (2022-02-03)


### Features

* **javascript array:** handle objects in `unique` function ([89fdff6](https://gitlab.com/hestia-earth/hestia-utils/commit/89fdff67648ccdae38d3d36953aa92cea672c5b7))

### [0.10.3](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.2...v0.10.3) (2021-09-07)


### Bug Fixes

* **lookup:** handle no column key ([10e374f](https://gitlab.com/hestia-earth/hestia-utils/commit/10e374f4e7b50156a542ba8a81d325a09537cc0d))

### [0.10.2](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.1...v0.10.2) (2021-09-03)


### Features

* **table:** add function to pivot `term.[@id](https://gitlab.com/id)` with `value` ([da2d87f](https://gitlab.com/hestia-earth/hestia-utils/commit/da2d87f732a20e31caaafbe98f95e5c3252e67a3))


### Bug Fixes

* **api:** handle download node with `original` state ([30fe659](https://gitlab.com/hestia-earth/hestia-utils/commit/30fe659ba5bb4295efef365b0a686ad516ec4ab7))

### [0.10.1](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.10.0...v0.10.1) (2021-08-18)


### Bug Fixes

* **storage azure:** handle resource not found ([3a68834](https://gitlab.com/hestia-earth/hestia-utils/commit/3a68834f6b6b4b8e91bf0e71abf18ac61b55b813))

## [0.10.0](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.9.2...v0.10.0) (2021-08-18)


### ⚠ BREAKING CHANGES

* **storage:** `_s3_client` has been moved under `storage`

### Features

* **storage:** handle Azure Blob Container storage ([200213e](https://gitlab.com/hestia-earth/hestia-utils/commit/200213e8677ee158587bd2db59ca4ff21ee2198d))

### [0.9.2](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.9.1...v0.9.2) (2021-08-12)


### Features

* **javascript:** export function to validate a list of terms ([7d68ef3](https://gitlab.com/hestia-earth/hestia-utils/commit/7d68ef347f2725f10ca93e24263550454e9e5a80))

### [0.9.1](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.9.0...v0.9.1) (2021-08-10)


### Bug Fixes

* **validate terms:** fix validation not running ([6bb57cd](https://gitlab.com/hestia-earth/hestia-utils/commit/6bb57cd4314c6920acc6e21d5daafbd28c5fa23b))

## [0.9.0](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.8.1...v0.9.0) (2021-08-07)


### ⚠ BREAKING CHANGES

* **python utils:** `keep_in_memory` for `download_lookup` is `True` by default
* **javascript:** `hestia-convert-csv` has been moved to `@hestia-eart/schema-convert` package

### Features

* **python utils:** set "keep in memory" for lookup to `True` by default ([94781e7](https://gitlab.com/hestia-earth/hestia-utils/commit/94781e7be576e6244e66babb20b93bccbcefd743))


### Bug Fixes

* **python api:** handle error on search no results ([230e232](https://gitlab.com/hestia-earth/hestia-utils/commit/230e23243f36f0a89cee1d0c81c7ee73de2455c1))


* **javascript:** remove `hestia-convert-csv` executable ([8b0dfa0](https://gitlab.com/hestia-earth/hestia-utils/commit/8b0dfa0472d98461f95bd6b8e236cad00b3c6f9a))

### [0.8.1](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.8.0...v0.8.1) (2021-07-17)


### Bug Fixes

* **python lookup:** handle all errors load lookup ([1cf8618](https://gitlab.com/hestia-earth/hestia-utils/commit/1cf861818d9c53043284154477eb61e494468bc6))

## [0.8.0](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.7.4...v0.8.0) (2021-07-16)


### Features

* **validate terms:** query all terms in single file at once ([aab5d66](https://gitlab.com/hestia-earth/hestia-utils/commit/aab5d66c1063c795c1c6a941abf04d2986bf15e0))

### [0.7.4](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.7.3...v0.7.4) (2021-07-15)


### Features

* **api:** add retry request on API calls ([c8690bd](https://gitlab.com/hestia-earth/hestia-utils/commit/c8690bd4096f159b33750c31be7e5785565a1d3b))

### [0.7.3](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.7.2...v0.7.3) (2021-07-13)


### Bug Fixes

* **python lookup:** handle any exception while getting value from table ([9da0b59](https://gitlab.com/hestia-earth/hestia-utils/commit/9da0b590074b4e8bb83dc074461b2fb5fa5dd36c))

### [0.7.2](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.7.1...v0.7.2) (2021-07-08)


### Features

* **python api:** handle node type as string in `download_hestia` ([0cfa778](https://gitlab.com/hestia-earth/hestia-utils/commit/0cfa7782abb863e6d151cf4eb59124335f9080ef))

### [0.7.1](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.7.0...v0.7.1) (2021-07-04)


### Features

* **validate terms:** handle `Actor` and `Organisation` ([b37e17c](https://gitlab.com/hestia-earth/hestia-utils/commit/b37e17ccac2194618d48da452db10e0ecad2ece0))

## [0.7.0](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.6.1...v0.7.0) (2021-07-04)


### Features

* **javascript:** add bin script to validate terms ([58e5df7](https://gitlab.com/hestia-earth/hestia-utils/commit/58e5df75566bd4701ddedfe3dd985a9ed2720669))

### [0.6.1](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.6.0...v0.6.1) (2021-07-04)


### Features

* **array:** add `isEqual` function ([9619842](https://gitlab.com/hestia-earth/hestia-utils/commit/96198421cf76e2070e8dd3bece335bf3bfb6b06a))


### Bug Fixes

* **convert csv:** handle different format of json content ([6eaf913](https://gitlab.com/hestia-earth/hestia-utils/commit/6eaf913844e05f9a497b1a58eca97715eeacd1de))

## [0.6.0](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.5.1...v0.6.0) (2021-06-02)


### ⚠ BREAKING CHANGES

* **python:** min requirement of schema is `4.0.0`

### Features

* **python:** use schema 4.0.0 ([06d1ac0](https://gitlab.com/hestia-earth/hestia-utils/commit/06d1ac0daae5a276e22771ebb22014b001967500))

### [0.5.1](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.5.0...v0.5.1) (2021-05-24)


### Bug Fixes

* **python lookup:** only handle strings for grouped data ([ed8a1f9](https://gitlab.com/hestia-earth/hestia-utils/commit/ed8a1f9064e57791ea3a51d0a4a5ef4d85985687))

## [0.5.0](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.4.10...v0.5.0) (2021-05-17)


### ⚠ BREAKING CHANGES

* **python:** hestia_earth.schema 3.5.0 required

### Features

* **python:** require schema 3.5.0 ([5d038f3](https://gitlab.com/hestia-earth/hestia-utils/commit/5d038f3efce182269a1211f5f3710a5a82719fc2))


### Bug Fixes

* **python api:** fix wrong path no dataState ([f055dcb](https://gitlab.com/hestia-earth/hestia-utils/commit/f055dcb8cf83b64910faa2de5329cac659be1c84))

### [0.4.10](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.4.9...v0.4.10) (2021-05-13)


### Features

* **javascript:** add script to convert jsonld/json to csv ([5944df8](https://gitlab.com/hestia-earth/hestia-utils/commit/5944df887dec7731734d3d1df1f46ef4b4f578d3))
* **javascript number:** export conversion units ([13fd367](https://gitlab.com/hestia-earth/hestia-utils/commit/13fd36798de95b0e2f2cb618c168595e975df7c6))
* **python tools:** add `flatten` method ([1ceb01e](https://gitlab.com/hestia-earth/hestia-utils/commit/1ceb01e2140f4a42c3b02b46836b0bd80ded304e))

### [0.4.9](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.4.8...v0.4.9) (2021-05-06)


### Features

* **python lookup:** add methods to extract grouped data ([fb6db32](https://gitlab.com/hestia-earth/hestia-utils/commit/fb6db3219ca92e90058293510d36d7dd44b46a24))

### [0.4.8](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.4.7...v0.4.8) (2021-04-29)


### Features

* **python api:** add raw `search` method ([7ffef61](https://gitlab.com/hestia-earth/hestia-utils/commit/7ffef61d3ff5dd298858ce40ec821e63bb7ab5d6))

### [0.4.7](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.4.6...v0.4.7) (2021-04-20)


### Features

* **python:** add average and sum methods on lists ([b071746](https://gitlab.com/hestia-earth/hestia-utils/commit/b07174688f400db55f5c502798f99e6186d3b8ab))
* **python model:** add `linked_node` method ([ab8d899](https://gitlab.com/hestia-earth/hestia-utils/commit/ab8d8994b3a0c050d8b7ea34f1a89fee4b12fa38))


### Bug Fixes

* **python date:** fix `is_in_days` not returning a boolean value ([344fb1d](https://gitlab.com/hestia-earth/hestia-utils/commit/344fb1d96f4b56a0444a3c2d45a47b83348b3a74))

### [0.4.6](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.4.5...v0.4.6) (2021-04-09)


### Bug Fixes

* **python tools:** does not round time in milliseconds ([a338896](https://gitlab.com/hestia-earth/hestia-utils/commit/a338896e55a7f018cc03cb6f16d8b1687ae49f6a))

### [0.4.5](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.4.4...v0.4.5) (2021-03-31)


### Features

* **python api:** handle passing access token for api requests ([2e7bb6a](https://gitlab.com/hestia-earth/hestia-utils/commit/2e7bb6a11a16e999eed3a893f2e03817928ae97b))

### [0.4.4](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.4.3...v0.4.4) (2021-03-26)


### Features

* **python api:** handle download node with `dataState` ([016a4ce](https://gitlab.com/hestia-earth/hestia-utils/commit/016a4ce3a36a12271608fd21d6c4830a881d6652))


### Bug Fixes

* **python api:** handle search for nested fields ([c5fe34a](https://gitlab.com/hestia-earth/hestia-utils/commit/c5fe34a33e627a8359072da8a14f20f46e5aeaf0))

### [0.4.3](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.4.2...v0.4.3) (2021-03-19)


### Bug Fixes

* **lookup:** handle missing int/float/string values ([afe23d4](https://gitlab.com/hestia-earth/hestia-utils/commit/afe23d48d3f6b679f3cb6de05d3651682c682637))

### [0.4.2](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.4.1...v0.4.2) (2021-03-19)


### Features

* **python lookup:** handle download without bucket env var ([9d48b0b](https://gitlab.com/hestia-earth/hestia-utils/commit/9d48b0b833a33fc032462fcc24297b943ab0e8eb))

### [0.4.1](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.4.0...v0.4.1) (2021-03-17)


### Features

* add function to convert a value from one unit to another ([c9f8b33](https://gitlab.com/hestia-earth/hestia-utils/commit/c9f8b33e6b18a90b88fe3aebd2ae7ceac8db78e9))

## [0.4.0](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.3.3...v0.4.0) (2021-03-15)


### ⚠ BREAKING CHANGES

* **csv:** sort option on `toCsv` has been removed.
Please use `headersFromCsv` to extract headers first, then `sortKeysByType`
from the schema library to sort the headers, and pass them to `toCsv` method

### Features

* **api:** add method `node_exists` ([9dd975a](https://gitlab.com/hestia-earth/hestia-utils/commit/9dd975a517559c4a04c4abf256a193d85e96f7d2))


### Bug Fixes

* **csv:** remove sorting of headers in favor of schema ([af61632](https://gitlab.com/hestia-earth/hestia-utils/commit/af61632449386f920b4f416ad03911280fea6517))

### [0.3.3](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.3.2...v0.3.3) (2021-03-09)


### Features

* **tools:** handle parse float `nan` ([78ea7cc](https://gitlab.com/hestia-earth/hestia-utils/commit/78ea7cc8f15696c714544c4a821dfa5517474224))


### Bug Fixes

* handle no value for `isIri` ([afb1195](https://gitlab.com/hestia-earth/hestia-utils/commit/afb11959ed66bb7d8363eb702fe9f9dae433ba46))

### [0.3.2](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.3.1...v0.3.2) (2021-02-27)


### Bug Fixes

* export date functions ([6c17ad8](https://gitlab.com/hestia-earth/hestia-utils/commit/6c17ad834f2ee6471aa5c8f34018aad0c5bf2bd3))

### [0.3.1](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.3.0...v0.3.1) (2021-02-27)


### Features

* **date:** add methods diff in days / years ([7d8d5f9](https://gitlab.com/hestia-earth/hestia-utils/commit/7d8d5f9d4d5cc19c1f9de6c7011d98b890ba20e4))
* **number:** add `toPrecision` function ([8a3bfc0](https://gitlab.com/hestia-earth/hestia-utils/commit/8a3bfc0619920af2e15c85f98d605169bcf8f0df))

## [0.3.0](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.2.1...v0.3.0) (2021-02-23)


### ⚠ BREAKING CHANGES

* **api:** `search_url` function has been removed

### Features

* **array:** add `intersection` function ([4512af7](https://gitlab.com/hestia-earth/hestia-utils/commit/4512af765f3172d1fe6a4dcf8d9840de952c31ad))
* **python:** update schema requirement to `2.0.0` ([3ae8b5f](https://gitlab.com/hestia-earth/hestia-utils/commit/3ae8b5f6ee09376c497a4f20efb3d932eba64e83))


* **api:** use api url for searching ([38f01a0](https://gitlab.com/hestia-earth/hestia-utils/commit/38f01a03a00e7e460845e31837e16712f6f6ea79))

### [0.2.1](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.2.0...v0.2.1) (2021-02-03)


### Bug Fixes

* **python api:** handle error message find related ([3e2475b](https://gitlab.com/hestia-earth/hestia-utils/commit/3e2475b7a0683331291607314f1e2f3efc8d5a39))

## [0.2.0](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.1.8...v0.2.0) (2021-02-01)


### Features

* **python:** add method to return time in ms ([3115d18](https://gitlab.com/hestia-earth/hestia-utils/commit/3115d1840addba36313328f22d02b5e6f3f3a7de))
* **python:** add safe parse float and date methods ([7c7fb55](https://gitlab.com/hestia-earth/hestia-utils/commit/7c7fb55a1c77af42f31abb3f018fb84a98f64001))
* **python lookup:** add option to store file in memory ([c10b259](https://gitlab.com/hestia-earth/hestia-utils/commit/c10b259a4e62616d84a47452f62f805084b39041))


### Bug Fixes

* **python utils:** fix default prod web url ([69ff382](https://gitlab.com/hestia-earth/hestia-utils/commit/69ff382ab3e157865f76e66bf4684032f5d559d4))

### [0.1.8](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.1.7...v0.1.8) (2021-01-22)


### Bug Fixes

* **python s3:** fix issue with too many requests in threads ([82477b2](https://gitlab.com/hestia-earth/hestia-utils/commit/82477b28b7efb2daadc0196edfafbb20d5edb22f))

### [0.1.7](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.1.6...v0.1.7) (2021-01-20)


### Features

* **javascript csv:** convert Date to JSON format ([d248836](https://gitlab.com/hestia-earth/hestia-utils/commit/d248836ab21d1d4088e6f3dd180312acb6ae8f8d))


### Bug Fixes

* **javascript csv:** handle GeoJSON as string or object ([3861494](https://gitlab.com/hestia-earth/hestia-utils/commit/38614941b8ae7a9310f73d550d0093b142e6e179))

### [0.1.6](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.1.5...v0.1.6) (2021-01-16)


### Features

* **javascript csv:** ad `toCsv` method ([a86ce90](https://gitlab.com/hestia-earth/hestia-utils/commit/a86ce90c892c50490c77913e1ebc0e6f18131503))

### [0.1.5](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.1.4...v0.1.5) (2021-01-14)


### Bug Fixes

* **python lookup:** fix lookup decode content ([4f46f3d](https://gitlab.com/hestia-earth/hestia-utils/commit/4f46f3d3cc861ec0bd423685da5fda64a9eef2c1))

### [0.1.4](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.1.3...v0.1.4) (2021-01-14)


### Features

* **api:** use get URL instead of download for better performance ([3fd2c59](https://gitlab.com/hestia-earth/hestia-utils/commit/3fd2c594ef444fc3d967435260c967fdf7af4b83))


### Bug Fixes

* **s3 client:** return raw data when loading from bucket ([418ce2b](https://gitlab.com/hestia-earth/hestia-utils/commit/418ce2b0b1bc550d90f9b0045201c7527740d038))

### [0.1.3](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.1.2...v0.1.3) (2021-01-12)


### Bug Fixes

* **python lookup:** fix lookup path on bucket ([d7f06e3](https://gitlab.com/hestia-earth/hestia-utils/commit/d7f06e35502ac34f61cfb39ab8da91e5e47217f3))

### [0.1.2](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.1.1...v0.1.2) (2021-01-07)


### Features

* **javascript:** add utils ([d727c9a](https://gitlab.com/hestia-earth/hestia-utils/commit/d727c9aeea04eb8c45a47d3d46915a3272a1ffe2))


### Bug Fixes

* **api:** handle no results when searching for node ([4f05d47](https://gitlab.com/hestia-earth/hestia-utils/commit/4f05d47563751aa8daff6da488c9723a619b61e3))

### [0.1.1](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.1.0...v0.1.1) (2020-12-23)


### Features

* **python:** add methods filter dict/list ([fffece9](https://gitlab.com/hestia-earth/hestia-utils/commit/fffece9bf04acb7a114439a9125979691067f6dc))

## [0.1.0](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.0.7...v0.1.0) (2020-12-17)


### ⚠ BREAKING CHANGES

* **python api:** method linked to API are now in `hestia_earth.utils.api`

### Features

* **python:** add method to download lookup from glossary ([44f9301](https://gitlab.com/hestia-earth/hestia-utils/commit/44f9301bc5d26e354c1675b81af49838265dfc59))


### Bug Fixes

* **python api:** handle errors on download node not found ([bce14fd](https://gitlab.com/hestia-earth/hestia-utils/commit/bce14fd68dce14f23922a2c6ec7f2bd543dda4eb))

### [0.0.7](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.0.6...v0.0.7) (2020-12-16)


### Bug Fixes

* **lookup:** handle `IndexError` for `get_table_value` ([cff8c0a](https://gitlab.com/hestia-earth/hestia-utils/commit/cff8c0afaea543b819db18b069ffa2bb6af9cade))

### [0.0.6](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.0.5...v0.0.6) (2020-12-15)


### Features

* **python:** add lookup table methods ([897502f](https://gitlab.com/hestia-earth/hestia-utils/commit/897502f2c717cfbecb555ff17b64a2c01b17d69d))

### [0.0.5](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.0.4...v0.0.5) (2020-12-10)


### Features

* **python:** improves speed of retrieving on s3 ([f8131ff](https://gitlab.com/hestia-earth/hestia-utils/commit/f8131ffb420d843bb46a53dca9fe3e6c0128b3ab))

### [0.0.4](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.0.3...v0.0.4) (2020-12-10)


### Features

* **python:** handle key not found on bucket ([318e2fd](https://gitlab.com/hestia-earth/hestia-utils/commit/318e2fd2e1a5e96ed1a0e08362713600e1612f2b))

### [0.0.3](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.0.2...v0.0.3) (2020-12-10)


### Features

* **python:** add method to get related nodes ([869a3c9](https://gitlab.com/hestia-earth/hestia-utils/commit/869a3c917370bb0950bc9945e44230baf628963d))


### Bug Fixes

* **python:** handle no term in `find_term_match` ([f547d74](https://gitlab.com/hestia-earth/hestia-utils/commit/f547d74eaa261b18655b5b542ad530f7703a481f))

### [0.0.2](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.0.1...v0.0.2) (2020-12-08)


### Bug Fixes

* **utils:** fix incorrect os env access ([3643f98](https://gitlab.com/hestia-earth/hestia-utils/commit/3643f98e76e35ca72c09d1fd51d37a4fd5824d97))

### 0.0.1 (2020-12-08)


### Features

* **python:** add functions to download and search nodes ([ee4040c](https://gitlab.com/hestia-earth/hestia-utils/commit/ee4040ca16748a2048d88beac9920214ae5c9fb1))
