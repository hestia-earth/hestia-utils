from hestia_earth.utils.blank_node import get_node_value


def test_get_node_value():
    blank_node = {'term': {'termType': 'crop', '@id': 'wheatGrain'}, 'value': [10]}
    assert get_node_value(blank_node, 'value', default=None) == 10

    blank_node = {'term': {'termType': 'crop', '@id': 'wheatGrain'}, 'value': [0]}
    assert get_node_value(blank_node, 'value', default=None) == 0

    blank_node = {'term': {'termType': 'crop', '@id': 'wheatGrain'}}
    assert get_node_value(blank_node, 'value', default=None) is None

    blank_node = {'term': {'termType': 'crop', '@id': 'wheatGrain'}, 'value': [10, 20]}
    assert get_node_value(blank_node, 'value', default=None) == 15

    blank_node = {'term': {'termType': 'crop', '@id': 'wheatGrain'}, 'value': True}
    assert get_node_value(blank_node, 'value', default=None) is True

    blank_node = {'term': {'termType': 'crop', '@id': 'wheatGrain'}, 'value': 10}
    assert get_node_value(blank_node, 'value', default=None) == 10

    blank_node = {'term': {'termType': 'crop', '@id': 'wheatGrain'}, 'value': None}
    assert get_node_value(blank_node, 'value', default=None) is None

    blank_node = {'term': {'termType': 'crop', '@id': 'wheatGrain'}, 'value': None}
    assert get_node_value(blank_node, 'value', default=0) == 0

    blank_node = {'term': {'termType': 'crop', '@id': 'wheatGrain'}, 'value': [10, None, 20]}
    assert get_node_value(blank_node, 'value', default=None) == 15
