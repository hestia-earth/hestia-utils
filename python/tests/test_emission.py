from unittest.mock import patch
from hestia_earth.schema import SiteSiteType, TermTermType

from hestia_earth.utils.emission import cycle_emissions_in_system_boundary, emissions_in_system_boundary

class_path = 'hestia_earth.utils.emission'


@patch(f"{class_path}.find_primary_product")
def test_cycle_emissions_in_system_boundary_cropland(mock_primary_product):
    cycle = {'site': {}}

    mock_primary_product.return_value = {
        'term': {
            '@id': 'wheatGrain',
            'termType': TermTermType.CROP.value
        }
    }
    cycle['site']['siteType'] = SiteSiteType.CROPLAND.value
    term_ids = cycle_emissions_in_system_boundary(cycle)
    assert len(term_ids) > 50

    mock_primary_product.return_value = {
        'term': {
            '@id': 'ricePlantFlooded',
            'termType': TermTermType.CROP.value
        }
    }
    cycle['site']['siteType'] = SiteSiteType.CROPLAND.value
    term_ids = cycle_emissions_in_system_boundary(cycle)
    assert len(term_ids) > 50

    # with inputs restriction, we should have less emissions
    cycle['inputs'] = [
        {
            'term': {'termType': 'crop'}
        }
    ]
    cycle['site']['siteType'] = SiteSiteType.CROPLAND.value
    assert len(cycle_emissions_in_system_boundary(cycle)) < len(term_ids)


@patch(f"{class_path}.find_primary_product")
def test_cycle_emissions_in_system_boundary_animal_housing(mock_primary_product):
    cycle = {'site': {}}

    mock_primary_product.return_value = {
        'term': {
            '@id': 'meatBeefCattleLiveweight',
            'termType': TermTermType.ANIMALPRODUCT.value
        }
    }
    cycle['site']['siteType'] = SiteSiteType.ANIMAL_HOUSING.value
    term_ids = cycle_emissions_in_system_boundary(cycle)
    assert len(term_ids) > 20


def test_emissions_in_system_boundary():
    term_ids = emissions_in_system_boundary()
    assert len(term_ids) > 50
