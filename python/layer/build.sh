#!/bin/sh

# exit when any command fails
set -e

export PKG_DIR="python"

LIBRARY="utils"
ROOT="./layer"
PKG_PATH="${ROOT}/$PKG_DIR/lib/python3.9/site-packages/"

rm -rf "${ROOT}/${PKG_DIR}" && mkdir -p "./layer/${PKG_DIR}"

PACKAGE_PATH="../package.json"
PACKAGE_VERSION=$(cat $PACKAGE_PATH \
  | grep version \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g' \
  | tr -d '[[:space:]]')

# create requirements.txt
cp requirements.txt "${ROOT}/requirements.txt"

docker run --rm -v $(pwd):/var/task public.ecr.aws/sam/build-python3.9 pip install -r "${ROOT}/requirements.txt" -t $PKG_PATH

# copy the current library
rm -rf "${PKG_PATH}hestia_earth/${LIBRARY}/"
mkdir -p "${PKG_PATH}hestia_earth"
cp -R "./hestia_earth/${LIBRARY}/" "${PKG_PATH}hestia_earth/${LIBRARY}"

# Removing nonessential files
find ${PKG_PATH} -type d -name "tests" -exec rm -r {} +
rm -rf ${PKG_PATH}*.dist-info

# remove numpy as already included in another layer
rm -rf ${PKG_PATH}numpy*
