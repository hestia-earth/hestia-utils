Welcome to HESTIA Utils's documentation!
========================================

This package contains convenience methods to use the HESTIA API or Search Engine or to manipulate the data.

Installation
------------

Install from `PyPI <https://pypi.python.org/pypi>`_ using `pip <http://www.pip-installer.org/en/latest/>`_, a
package manager for Python.

.. code-block:: bash

    pip install hestia_earth.utils


Requirements
============

- `hestia_earth.schema >= 2.8.0 <https://pypi.org/project/hestia-earth.schema/>`_
- `requests >= 2.24.0 <https://pypi.org/project/requests/>`_

Contents
--------

.. autosummary::
   :toctree: _autosummary
   :caption: API Reference
   :template: custom-module-template.rst
   :recursive:

   hestia_earth.utils


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
