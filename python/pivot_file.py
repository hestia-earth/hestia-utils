from dotenv import load_dotenv
load_dotenv()

import os
import requests
import json
import argparse
from hestia_earth.utils.tools import current_time_ms, flatten
from hestia_earth.utils.pivot.pivot_csv import pivot_nodes, nodes_to_df
from hestia_earth.utils.api import _safe_get_request, node_type_to_url


parser = argparse.ArgumentParser(
    description='Pivot file uploaded on the hestia platform')
parser.add_argument('--file-id', type=str, required=True,
                    help='ID of the file on Hestia.')
parser.add_argument('--beta', action='store_true',
                    help='Use the DATA Api to download available nodes.')
args = parser.parse_args()


API_URL = os.getenv('API_URL')
DATA_API_URL = os.getenv('DATA_API_URL')

file_data = _safe_get_request(f"{API_URL}/files/{args.file_id}")
data = _safe_get_request(file_data.get('dataPath'))
nodes = data.get('nodes', [])
filename = file_data.get('filename').split('.')[0]
output_file = f"samples/{filename}-compacted.csv"


def run_by_nodes(): return pivot_nodes(nodes)


def _download_nodes(url: str, ids: list[str]):
    headers = {'Content-Type': 'application/json'}
    return requests.post(url, json.dumps({'ids': ids}), headers=headers).json().get('results', []) if ids else []


def _download_from_api(node_type: str):
    ids = [n.get('@id') for n in nodes if n.get('@type') == node_type and not n.get('aggregated', False)]
    aggregated_ids = [n.get('@id') for n in nodes if n.get('@type') == node_type and n.get('aggregated', False)]
    return flatten([
        _download_nodes(f"{DATA_API_URL}/nodes/{node_type_to_url(node_type)}-by-id", ids),
        _download_nodes(f"{DATA_API_URL}/aggregated-nodes/{node_type_to_url(node_type)}-by-id", aggregated_ids)
    ])


def run_from_data_api():
    return nodes_to_df(flatten(map(_download_from_api, ['Cycle', 'Site', 'ImpactAssessment', 'Source'])))


def run():
    now = current_time_ms()
    df = run_from_data_api() if args.beta else run_by_nodes()
    df.to_csv(output_file, index=None)
    print('Done pivoting in', (current_time_ms() - now) / 1000, 's')


run()
