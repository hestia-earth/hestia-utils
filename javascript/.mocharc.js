'use strict'

module.exports = {
  require: [
    'ts-node/register',
    'source-map-support/register'
  ],
  'full-trace': true,
  timeout: 30000
}
