import { promises } from 'fs';
import { join } from 'path';
import { request } from 'https';

import { unique } from './array';

const API_URL = process.env.API_URL;

const [folder, extension] = process.argv.slice(2);

const { readdir, lstat, readFile } = promises;
const encoding = 'utf8';

class Term {
  '@id': string;
  name: string;
}

const search = (body: any) =>
  new Promise<any>((resolve, reject) => {
    const req = request(
      `${API_URL}/search`,
      {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' }
      },
      res => {
        res.setEncoding(encoding);
        const data = [];
        res.on('data', chunk => data.push(chunk));
        res.on('end', () => resolve(JSON.parse(data.join(''))));
        res.on('error', reject);
      }
    );
    req.write(JSON.stringify(body));
    req.end();
  });

const recursiveFiles = async (directory: string): Promise<string[]> =>
  (
    await Promise.all(
      (await readdir(directory)).map(async entry =>
        (await lstat(join(directory, entry))).isDirectory()
          ? await recursiveFiles(join(directory, entry))
          : join(directory, entry).endsWith(`.${extension || 'jsonld'}`)
          ? [join(directory, entry)]
          : []
      )
    )
  ).flat();

const getAllTerms = (data): Term[] =>
  Object.keys(data)
    .filter(key => Array.isArray(data[key]) || (typeof data[key] === 'object' && '@type' in data[key]))
    .flatMap(key => {
      const value = data[key];
      const isArray = Array.isArray(value);
      return isArray ? value.flatMap(getAllTerms) : value['@type'] === 'Term' ? value : getAllTerms(value);
    })
    .filter(term => !!term && term['@type'] === 'Term' && (!!term['@id'] || !!term.name));

// search is limited to 1000 parameters
const searchLimit = 500;

const searchTerms = async (terms: Term[]) => {
  const searchedTerms = terms.slice(0, searchLimit);
  const { results } = searchedTerms.length
    ? await search({
        limit: searchLimit,
        fields: ['@id', 'name'],
        query: {
          bool: {
            must: [
              {
                match: { '@type': 'Term' }
              }
            ],
            should: searchedTerms.map(({ '@id': id, name }) => ({
              match: id ? { '@id.keyword': id } : { 'name.keyword': name }
            })),
            minimum_should_match: 1
          }
        }
      })
    : { results: [] };
  return [...results, ...(terms.length > searchLimit ? await searchTerms(terms.slice(searchLimit)) : [])];
};

/**
 * Validate a list of Terms.
 *
 * @param terms The list of Terms to validate.
 * @returns The list of ids/names that could not be found (invalid).
 */
export const validateTerms = async (terms: Term[]) => {
  const results = await searchTerms(terms);
  return terms
    .map(({ '@id': id, name }) => {
      const exists = results.find(res => res['@id'] === id || res.name === name);
      return exists ? true : id || name;
    })
    .filter(val => val !== true);
};

const validateFile =
  (existingTerms: Term[]) =>
  ({ filepath, terms }) => ({
    filepath,
    missing: terms
      .map(({ '@id': id, name }) =>
        existingTerms.find(res => res['@id'] === id || res.name === name) ? true : id || name
      )
      .filter(val => val !== true)
  });

const loadFile = async filepath => ({
  filepath,
  terms: getAllTerms(JSON.parse(await readFile(filepath, encoding)))
});

export const run = async () => {
  const filepaths = await recursiveFiles(folder);
  const fileData = await Promise.all(filepaths.map(loadFile));
  const allTerms = unique(fileData.flatMap(({ terms }) => terms));
  const existingTerms = await searchTerms(allTerms);
  const results = fileData.map(validateFile(existingTerms)).filter(Boolean);
  const missingFiles = results.filter(({ missing }) => missing.length);
  if (missingFiles.length) {
    throw new Error(
      `
  Terms not found:
  \t${missingFiles.map(({ filepath, missing }) => `- ${filepath}: ${missing.join(', ')}`).join('\n\t')}
    `.trim()
    );
  }
  return filepaths;
};
