import { propertyValue } from './term';

export enum DeltaDisplayType {
  absolute = 'absolute',
  percent = 'percent'
}

type deltaTypeMapping = {
  [type in DeltaDisplayType]?: (value: number, original: number) => number;
};

const deltaPerType: deltaTypeMapping = {
  [DeltaDisplayType.absolute]: (value, original) => value - original,
  [DeltaDisplayType.percent]: (value, original) => ((value - original) / original) * 100
};

const roundValue = (value: any) => +`${value}`.substring(0, 10);

enum PercentDeltaConditions {
  recalculated0 = 'recalculated should be 0',
  original0 = 'original is 0'
}

const calculatePercentDeltaConditions: {
  [key in PercentDeltaConditions]: (original: number, recalculated: number) => boolean;
} = {
  [PercentDeltaConditions.recalculated0]: (original, recalculated) => original > 0 && recalculated === 0,
  [PercentDeltaConditions.original0]: (original, recalculated) => original === 0 && recalculated > 0
};

const calculatePercentDeltaResult: {
  [key in PercentDeltaConditions | 'default']: (original: number, recalculated: number) => number;
} = {
  [PercentDeltaConditions.recalculated0]: (original, recalculated) => (recalculated - original) / (original + 1),
  // Always considered an error so deliberately exceed SUCCESS_CRITERION_MAX_DELTA_PERCENT
  [PercentDeltaConditions.original0]: (original, recalculated) => Math.sign(recalculated - original),
  default: (original, recalculated) => (recalculated - original) / original
};

const calculatePercentDelta = (recalculated: number, original: number) => {
  const matchingCondition =
    Object.values(PercentDeltaConditions).find(value =>
      calculatePercentDeltaConditions[value](original, recalculated)
    ) || 'default';
  return calculatePercentDeltaResult[matchingCondition](original, recalculated) * 100;
};

export const customDeltaFuncs = {
  [DeltaDisplayType.percent]: calculatePercentDelta
};

/**
 * Calculate the delta between 2 values.
 *
 * @param value
 * @param originalValue
 * @param displayType
 * @param mapping
 * @param termId If calculating delta for a specific Term.
 * @returns
 */
export const delta = (
  value: any,
  originalValue: any,
  displayType = DeltaDisplayType.percent,
  mapping?: deltaTypeMapping,
  termId?: string
) => {
  const vvalue = roundValue(propertyValue(value, termId));
  const voriginalValue = roundValue(propertyValue(originalValue, termId));
  const deltaFuncs = { ...deltaPerType, ...mapping };
  const diff = vvalue === voriginalValue ? 0 : deltaFuncs[displayType](vvalue, voriginalValue);
  return Number.isFinite(diff) ? (diff === -0 ? 0 : diff) : 0;
};
