export const secondMs = 1000;
export const minuteMs = 60 * secondMs;
export const hourMs = 60 * minuteMs;
export const dayMs = 24 * hourMs;
const year = 365.2425;

/**
 * Get the difference in days between two dates.
 *
 * @param date1 The left date
 * @param date2 The right date
 *
 * @returns The difference between date1 and date2 in days (rounded).
 */
export const diffInDays = (date1: Date | string, date2: Date | string) =>
  Math.abs(Math.round((new Date(date2).getTime() - new Date(date1).getTime()) / dayMs));

/**
 * Get the difference in years between two dates.
 *
 * @param date1 The left date
 * @param date2 The right date
 *
 * @returns The difference between date1 and date2 in years (precision: 1).
 */
export const diffInYears = (date1: Date | string, date2: Date | string) =>
  Math.round((diffInDays(date1, date2) / year) * 10) / 10;

/**
 * Remove months on the date.
 *
 * @param date The date.
 * @param days How many months to remove. Note: you can use a negative number to add months.
 *
 * @returns New date.
 */
export const monthsBefore = (date = new Date(), months = 1) => {
  const newDate = new Date(date);
  newDate.setMonth(date.getMonth() - months);
  return newDate;
};

/**
 * Remove days on the date.
 *
 * @param date The date.
 * @param days How many days to remove. Note: you can use a negative number to add days.
 *
 * @returns New date.
 */
export const daysBefore = (date = new Date(), days = 1) => {
  const newDate = new Date(date);
  newDate.setDate(date.getDate() - days);
  return newDate;
};

/**
 * Remove hours on the date.
 *
 * @param date The date.
 * @param minutes How many hours to remove. Note: you can use a negative number to add hours.
 *
 * @returns New date.
 */
export const hoursBefore = (date = new Date(), hours = 1) => {
  const newDate = new Date(date);
  newDate.setHours(date.getHours() - hours);
  return newDate;
};

/**
 * Remove minutes on the date.
 *
 * @param date The date.
 * @param minutes How many minutes to remove. Note: you can use a negative number to add minutes.
 *
 * @returns New date.
 */
export const minutesBefore = (date = new Date(), minutes = 1) => {
  const newDate = new Date(date);
  newDate.setMinutes(date.getMinutes() - minutes);
  return newDate;
};
