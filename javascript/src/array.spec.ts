import { expect } from 'chai';
import 'mocha';

import { intersection, unique, isEqual } from './array';

describe('array', () => {
  describe('intersection', () => {
    it('should select the intersection', () => {
      const array1 = [1, 2, 3, 4];
      const array2 = [2, 4, 5, 6];
      expect(intersection(array1, array2)).to.deep.equal([2, 4]);
    });
  });

  describe('unique', () => {
    describe('list without objects', () => {
      it('should return unique values', () => {
        const values = [1, 2, 2, '4', true, true];
        expect(unique(values)).to.deep.equal([1, 2, '4', true]);
      });
    });

    describe('list with objects', () => {
      it('should return unique values', () => {
        const values = [1, 2, 2, { a: 1 }, { a: 1 }, { a: 2 }];
        expect(unique(values)).to.deep.equal([1, 2, { a: 1 }, { a: 2 }]);
      });
    });
  });

  describe('isEqual', () => {
    const array1 = [1, 'a', 2, 'd'];

    it('should not be equal', () => {
      expect(isEqual(array1, [1, 2, 'd'])).to.equal(false);
    });

    it('should be equal', () => {
      expect(isEqual(array1, array1)).to.equal(true);
    });
  });
});
