/**
 * Return list of unique values.
 *
 * @param values List of values
 */
export const unique = <T>(values: T[]): T[] =>
  values.some(v => typeof v === 'object')
    ? [...new Set(values.map(v => JSON.stringify(v)))].map(v => JSON.parse(v))
    : [...new Set(values)];

/**
 * Return an array containing all values that appear in both arrays.
 *
 * @param array1 List of values
 * @param array2 List of values
 */
export const intersection = <T>(array1: T[], array2: T[]) => array1.filter(x => array2.includes(x));

/**
 * Checks if both arrays are equal.
 *
 * @param array1 List of values
 * @param array2 List of values
 */
export const isEqual = <T>(array1: T[], array2: T[]) => JSON.stringify(array1) === JSON.stringify(array2);
