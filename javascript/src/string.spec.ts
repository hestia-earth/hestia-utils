import { expect } from 'chai';
import 'mocha';

import { ellipsis, keyToLabel, toDashCase, now } from './string';

describe('string', () => {
  describe('ellipsis', () => {
    it('should truncate long text', () => {
      expect(ellipsis('some very long text', 10)).to.equal('some very ...');
    });

    it('should not truncate short text', () => {
      expect(ellipsis('some text')).to.equal('some text');
    });

    it('should ignore no text', () => {
      expect(ellipsis()).to.equal('');
    });
  });

  describe('keyToLabel', () => {
    it('shuld transform as label', () => {
      expect(keyToLabel('other natual vegetation')).to.equal('Other natual vegetation');
      expect(keyToLabel('randomKey_with_chars')).to.equal('Random Key With Chars');
    });
  });

  describe('toDashCase', () => {
    it('should handle no value', () => {
      expect(toDashCase()).to.equal(null);
    });

    it('should handle term ids', () => {
      expect(toDashCase('n2OToAirNaturalVegetationBurningDirect')).to.equal(
        'n2o-to-air-natural-vegetation-burning-direct'
      );

      expect(toDashCase('nh3ToAirCropResidueBurning')).to.equal('nh3-to-air-crop-residue-burning');

      expect(toDashCase('co2ToAirSoilCarbonStockChange')).to.equal('co2-to-air-soil-carbon-stock-change');
      expect(toDashCase('gwp100')).to.equal('gwp100');
      expect(toDashCase('marineEutrophicationPotential')).to.equal('marine-eutrophication-potential');
      expect(toDashCase('akagiEtAl2011AndIpcc2006')).to.equal('akagi-et-al-2011-and-ipcc-2006');
      expect(toDashCase('emeaEea2019')).to.equal('emea-eea-2019');

      expect(toDashCase('landTransformationFromCropland100YearAverageInputsProduction')).to.equal(
        'land-transformation-from-cropland-100-year-average-inputs-production'
      );

      expect(toDashCase('ipcc2006')).to.equal('ipcc-2006');
    });

    it('should handle model as term types', () => {
      expect(toDashCase('impact_assessment')).to.equal('impact-assessment');
      expect(toDashCase('input.properties')).to.equal('input-properties');
    });
  });

  describe('now', () => {
    it('should create a string', () => {
      expect(now().length).to.equal(10);
    });
  });
});
