import { expect } from 'chai';
import 'mocha';

import { diffInDays, diffInYears, monthsBefore, daysBefore, hoursBefore, minutesBefore } from './date';

describe('date', () => {
  describe('diffInDays', () => {
    it('should return the difference in days', () => {
      expect(diffInDays('04/02/2014', '11/04/2014')).to.equal(216);
      expect(diffInDays('12/02/2014', '11/04/2014')).to.equal(28);
    });
  });

  describe('diffInYears', () => {
    it('should return the difference in days', () => {
      expect(diffInYears('04/02/2014', '11/04/2018')).to.equal(4.6);
      expect(diffInYears('12/02/2018', '11/04/2016')).to.equal(2.1);
    });
  });

  describe('monthsBefore', () => {
    it('should return a date', () => {
      expect(monthsBefore()).to.be.instanceOf(Date);
    });

    it('should remove months to date', () => {
      const date = new Date(2000, 1, 2, 12, 0, 0);
      const newDate = monthsBefore(date, 1);
      expect(date.getMonth() - newDate.getMonth()).to.equal(1);
    });
  });

  describe('daysBefore', () => {
    it('should return a date', () => {
      expect(daysBefore()).to.be.instanceOf(Date);
    });

    it('should remove days to date', () => {
      const date = new Date(2000, 1, 2, 12, 0, 0);
      const newDate = daysBefore(date, 1);
      expect(date.getDate() - newDate.getDate()).to.equal(1);
    });
  });

  describe('hoursBefore', () => {
    it('should return a date', () => {
      expect(hoursBefore()).to.be.instanceOf(Date);
    });

    it('should remove hours to date', () => {
      const date = new Date(2000, 1, 2, 12, 0, 0);
      const newDate = hoursBefore(date, 1);
      expect(date.getHours() - newDate.getHours()).to.equal(1);
    });
  });

  describe('minutesBefore', () => {
    it('should return a date', () => {
      expect(minutesBefore()).to.be.instanceOf(Date);
    });

    it('should remove minutes to date', () => {
      const date = new Date(2000, 1, 2, 12, 1, 0);
      const newDate = minutesBefore(date, 1);
      expect(date.getMinutes() - newDate.getMinutes()).to.equal(1);
    });
  });
});
