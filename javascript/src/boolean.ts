export const isBoolean = (value?: any) =>
  typeof value === 'boolean' || `${value}`.toLowerCase() === 'true' || `${value}`.toLowerCase() === 'false';
