import { expect } from 'chai';
import * as sinon from 'sinon';
import 'mocha';

import * as glossary from '@hestia-earth/glossary';
import * as utils from './utils';
import { arrayValue, propertyValue, emptyValue } from './term';
import * as specs from './term';

let stubs: sinon.SinonStub[] = [];

describe('term', () => {
  beforeEach(() => {
    stubs = [];
  });

  afterEach(() => {
    stubs.forEach(stub => stub.restore());
  });

  describe('arrayValue', () => {
    describe('no values', () => {
      it('should return null', () => {
        expect(arrayValue()).to.equal(null);
      });
    });

    describe('all undefined values', () => {
      it('should return null', () => {
        expect(arrayValue()).to.equal(null);
      });
    });

    describe('no averaging', () => {
      const values = ['100', 200, 300];

      it('should sum the values', () => {
        expect(arrayValue(values)).to.equal(600);
        expect(arrayValue(values, false)).to.equal(600);
      });
    });

    describe('with averaging', () => {
      const values = ['100', 200, 300];

      it('should mean the values', () => {
        expect(arrayValue(values, true)).to.equal(200);
      });
    });

    describe('with boolean values', () => {
      it('should return a single value', () => {
        expect(arrayValue([true, false, false])).to.equal(false);
        expect(arrayValue([true, false, 'false'])).to.equal(false);
        expect(arrayValue([true, true, true])).to.equal(true);
      });
    });

    describe('with string value', () => {
      it('should return a joined string', () => {
        expect(arrayValue(['a', 'b'])).to.equal('a;b');
      });
    });
  });

  describe('propertyValue', () => {
    let termId: string;

    describe('no value', () => {
      it('should return null', () => {
        expect(propertyValue(undefined)).to.equal(null);
        expect(propertyValue(null)).to.equal(null);
      });
    });

    describe('value is an array of numbers', () => {
      const value = ['100', 200, 300, null];

      describe('with a term ID', () => {
        beforeEach(() => {
          termId = 'termId';
        });

        describe('when array treament is mean', () => {
          beforeEach(() => {
            stubs.push(sinon.stub(glossary, 'getArrayTreatment').returns('mean'));
          });

          it('should return the mean', () => {
            expect(propertyValue(value, termId)).to.equal(200);
          });

          it('should handle array of zeros', () => {
            expect(propertyValue([0, 0, 0], termId)).to.equal(0);
          });
        });

        describe('when array treament is not mean', () => {
          beforeEach(() => {
            stubs.push(sinon.stub(glossary, 'getArrayTreatment').returns(''));
          });

          it('should return the sum', () => {
            expect(propertyValue(value, termId)).to.equal(600);
          });
        });
      });

      describe('without a term id', () => {
        it('should return the sum', () => {
          expect(propertyValue(value)).to.equal(600);
        });
      });
    });

    describe('value is not an array', () => {
      it('should return a single value', () => {
        expect(propertyValue('100', termId)).to.equal(100);
        expect(propertyValue('test', termId)).to.equal('test');
      });
    });
  });

  describe('emptyValue', () => {
    const value = null;

    describe('value is empty', () => {
      beforeEach(() => {
        stubs.push(sinon.stub(utils, 'isEmpty').returns(true));
      });

      it('should return true', () => {
        expect(emptyValue(value)).to.equal(true);
      });
    });

    describe('value is not empty', () => {
      beforeEach(() => {
        stubs.push(sinon.stub(utils, 'isEmpty').returns(false));
      });

      describe('property value is a number', () => {
        beforeEach(() => {
          stubs.push(sinon.stub(specs, 'propertyValue').returns(10));
        });

        it('should return false', () => {
          expect(emptyValue(value)).to.equal(false);
        });
      });

      describe('property value is not a number', () => {
        beforeEach(() => {
          stubs.push(sinon.stub(specs, 'propertyValue').returns(NaN));
        });

        it('should return true', () => {
          expect(emptyValue(value)).to.equal(true);
        });
      });
    });
  });
});
