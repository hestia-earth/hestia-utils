/**
 * Check if the value is a number.
 *
 * @param n Number as string or number
 * @returns true if the value is a number, false otherwise
 */
export const isNumber = (n: string | number) =>
  [/^(-)?[\d\.]+((e|e-)[\d]+)?$/.exec(`${n}`)?.length, !isNaN(parseFloat(`${n}`)), isFinite(parseFloat(`${n}`))].every(
    Boolean
  );

/**
 * Returns a number with significant figures.
 * Example: 3645.875 with 3 significant figures will return 3650.
 *
 * @param n The value
 * @param precision The number of significant figures
 */
export const toPrecision = (n: number, precision = 3) => {
  const mult = Math.pow(10, precision - Math.floor(Math.log(n < 0 ? -n : n) / Math.LN10) - 1);
  const multiplier = Math.max(
    // handle floating errors like 0.00009999999999999999
    +`${mult}`.replace(/[0]([1-9])\1+/g, v => `${+v.substring(0, 1) + 1}`),
    // dividing by 0.00001 will give floating point error
    0.0001
  );
  return n === 0 ? 0 : Math.round(n * multiplier) / multiplier;
};

/**
 * Returns the number formatted with commas every thousand.
 *
 * @param n The value
 */
export const toComma = (n: number) => {
  const [numberPart, decimalPart] = n.toString().split('.');
  const thousands = /\B(?=(\d{3})+(?!\d))/g;
  return `${numberPart.replace(thousands, ',')}${decimalPart ? `.${decimalPart}` : ''}`;
};

const C = 12.012;
const CA = 40.078;
const H = 1.008;
const K = 39.098;
const N = 14.007;
const O = 15.999;
const P = 30.974;

export enum ConvertUnits {
  m3 = 'm3',
  kg = 'kg',
  L = 'L',
  MJ = 'MJ',
  kWh = 'kWh',
  kgCa = 'kg Ca',
  kgCaCO3 = 'kg CaCO3',
  kgCaO = 'kg CaO',
  kgCaMg_CO3_2 = 'kg CaMg(CO3)2',
  kgCa_OH_2 = 'kg Ca(OH)2',
  kgCH4 = 'kg CH4',
  kgCH4C = 'kg CH4-C',
  kgCO2 = 'kg CO2',
  kgCO2C = 'kg CO2-C',
  kgK = 'kg K',
  kgK2O = 'kg K2O',
  kgMgCO3 = 'kg MgCO3',
  kgN2 = 'kg N2',
  kgN2N = 'kg N2-N',
  kgN2O = 'kg N2O',
  kgN2ON = 'kg N2O-N',
  kgNH3 = 'kg NH3',
  kgNH3N = 'kg NH3-N',
  kgNH4 = 'kg NH4',
  kgNH4N = 'kg NH4-N',
  kgNO2 = 'kg NO2',
  kgNO2N = 'kg NO2-N',
  kgNO3 = 'kg NO3',
  kgNO3N = 'kg NO3-N',
  kgNOx = 'kg NOx',
  kgNOxN = 'kg NOx-N',
  kgP = 'kg P',
  kgP2O5 = 'kg P2O5',
  kgPO43 = 'kg PO43'
}

export interface IConvertArgs {
  density?: number;
}

export const converters: {
  [fromUnit in ConvertUnits]?: {
    [toUnit in ConvertUnits]?: (value: number, args: IConvertArgs) => number;
  };
} = {
  [ConvertUnits.m3]: {
    [ConvertUnits.kg]: (value, args) => value * args.density,
    [ConvertUnits.L]: value => value * 1000
  },
  [ConvertUnits.kg]: {
    [ConvertUnits.m3]: (value, args) => value / args.density,
    [ConvertUnits.L]: (value, args) => (value / args.density) * 1000
  },
  [ConvertUnits.L]: {
    [ConvertUnits.kg]: (value, args) => (value * args.density) / 1000,
    [ConvertUnits.m3]: value => value / 1000
  },
  [ConvertUnits.kWh]: {
    [ConvertUnits.MJ]: value => value * 3.6
  },
  [ConvertUnits.MJ]: {
    [ConvertUnits.kWh]: value => value / 3.6
  },
  [ConvertUnits.kgP]: {
    [ConvertUnits.kgP2O5]: value => (value * (P * 2)) / (P * 2 + O * 5),
    [ConvertUnits.kgPO43]: value => (value * P) / ((P + O * 4) * 3)
  },
  [ConvertUnits.kgPO43]: {
    [ConvertUnits.kgP2O5]: value => (value * ((P + O * 4) * 3)) / (P * 2 + O * 5)
  },
  [ConvertUnits.kgK]: {
    [ConvertUnits.kgK2O]: value => (value * (K * 2)) / (K * 2 + O)
  },
  [ConvertUnits.kgCa]: {
    [ConvertUnits.kgCaO]: value => (value * CA) / (CA + O)
  },
  [ConvertUnits.kgCaO]: {
    [ConvertUnits.kgCaCO3]: value => (value * (CA + O)) / (CA + C + O * 3)
  },
  [ConvertUnits.kgCaMg_CO3_2]: {
    [ConvertUnits.kgCaCO3]: value => value
  },
  [ConvertUnits.kgCa_OH_2]: {
    [ConvertUnits.kgCaCO3]: value => value
  },
  [ConvertUnits.kgCaCO3]: {
    [ConvertUnits.kgCO2]: value => value * 0.12
  },
  [ConvertUnits.kgMgCO3]: {
    [ConvertUnits.kgCO2]: value => value * 0.13
  },
  [ConvertUnits.kgCH4]: {
    [ConvertUnits.kgCH4C]: value => (value * (C + H * 4)) / C
  },
  [ConvertUnits.kgCO2]: {
    [ConvertUnits.kgCO2C]: value => (value * (C + O * 2)) / C
  },
  [ConvertUnits.kgNOx]: {
    [ConvertUnits.kgNOxN]: value => (value * (N + O)) / N
  },
  [ConvertUnits.kgN2]: {
    [ConvertUnits.kgN2N]: value => value * 1
  },
  [ConvertUnits.kgN2O]: {
    [ConvertUnits.kgN2ON]: value => (value * (N * 2 + O)) / (N * 2)
  },
  [ConvertUnits.kgNO2]: {
    [ConvertUnits.kgNO2N]: value => (value * (N + O * 2)) / N
  },
  [ConvertUnits.kgNO3]: {
    [ConvertUnits.kgNO3N]: value => (value * (N + O * 3)) / N
  },
  [ConvertUnits.kgNH3]: {
    [ConvertUnits.kgNH3N]: value => (value * (N + H * 3)) / N
  },
  [ConvertUnits.kgNH4]: {
    [ConvertUnits.kgNH4N]: value => (value * (N + H * 4)) / N
  }
};

/**
 * Converts a value of unit into a different unit.
 * Depending on the destination unit, additional arguments might be provided.
 *
 * @param n The value to convert, usually a float or an integer.
 * @param fromUnit The unit the value is specified in.
 * @param toUnit The unit the converted value should be.
 * @param args Optional arguments to provide depending on the conversion.
 * @returns The converted value.
 */
export const convertValue = (n: number, fromUnit: ConvertUnits, toUnit: ConvertUnits, args?: IConvertArgs): number =>
  converters[fromUnit][toUnit](n, args);
