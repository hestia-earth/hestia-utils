import { expect } from 'chai';
import 'mocha';

import { isBoolean } from './boolean';

describe('boolean', () => {
  describe('isBoolean', () => {
    describe('boolean values', () => {
      it('should return true', () => {
        expect(isBoolean('TRUE')).to.equal(true);
        expect(isBoolean('false')).to.equal(true);
        expect(isBoolean(true)).to.equal(true);
        expect(isBoolean(false)).to.equal(true);
      });
    });

    describe('non boolean values', () => {
      it('should return false', () => {
        expect(isBoolean('abc')).to.equal(false);
        expect(isBoolean('abc123')).to.equal(false);
        expect(isBoolean('123')).to.equal(false);
      });
    });

    describe('no value', () => {
      it('should return false', () => {
        expect(isBoolean()).to.equal(false);
        expect(isBoolean(null)).to.equal(false);
        expect(isBoolean(undefined)).to.equal(false);
        expect(isBoolean('')).to.equal(false);
        expect(isBoolean([])).to.equal(false);
        expect(isBoolean({})).to.equal(false);
      });
    });
  });
});
