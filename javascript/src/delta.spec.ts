import { expect } from 'chai';
import * as sinon from 'sinon';
import 'mocha';

import * as term from './term';
import { customDeltaFuncs, delta, DeltaDisplayType } from './delta';

let stubs: sinon.SinonStub[] = [];

describe('delta', () => {
  beforeEach(() => {
    stubs = [];
  });

  afterEach(() => {
    stubs.forEach(stub => stub.restore());
  });

  describe('delta', () => {
    const originalValues = [200];
    const newValues = [150];

    beforeEach(() => {
      stubs.push(sinon.stub(term, 'propertyValue').callsFake(v => (v as number[]).reduce((p, c) => p + c, 0)));
    });

    describe('identical values', () => {
      it('should return 0', () => {
        expect(Math.round(delta(originalValues, originalValues))).to.equal(0);
      });
    });

    describe('0 values', () => {
      it('should return 0', () => {
        expect(Math.round(delta([0], [0], DeltaDisplayType.absolute))).to.equal(0);
      });
    });

    describe('without custom mappings', () => {
      describe('display as percentage', () => {
        const displayType = DeltaDisplayType.percent;

        it('should return the delta', () => {
          expect(Math.round(delta(newValues, originalValues, displayType))).to.equal(-25);
        });
      });

      describe('display as absolute', () => {
        const displayType = DeltaDisplayType.absolute;

        it('should return the delta', () => {
          expect(delta(newValues, originalValues, displayType)).to.equal(-50);
        });
      });
    });

    describe('with custom mappings', () => {
      const mappings = customDeltaFuncs;

      describe('display as percentage', () => {
        const displayType = DeltaDisplayType.percent;

        it('should return the delta', () => {
          expect(Math.round(delta(newValues, originalValues, displayType, mappings))).to.equal(-25);
        });

        describe('original is 0', () => {
          it('should return the delta', () => {
            expect(Math.round(delta(newValues, [0], displayType, mappings))).to.equal(100);
          });
        });

        describe('new is 0', () => {
          it('should return the delta', () => {
            expect(Math.round(delta([0], originalValues, displayType, mappings))).to.equal(-100);
          });
        });
      });
    });
  });
});
