/**
 * Trims the string to a certain max length and replace with `...`.
 *
 * @param text The text to ellipsize.
 * @param maxlength The maximum length of the text (including `...`).
 * @returns
 */
export const ellipsis = (text = '', maxlength = 20) =>
  text.length > maxlength ? `${text.substring(0, maxlength)}...` : text;

export const keyToLabel = (key: string) =>
  `${key[0].toUpperCase()}${key
    .replace(/([a-z])([A-Z])/g, '$1 $2')
    .replace(/([_])([a-zA-Z])/g, g => ` ${g[1].toUpperCase()}`)
    .substring(1)}`;

/**
 * Convert a Term ID into a dash string. Use it to generate link to documentation.
 *
 * @param value The original value.
 * @returns A dash case version of the value.
 */
export const toDashCase = (value?: string) =>
  value
    ? value
        // handle dates followed by capital letter
        .replace(/([\d]{4})([A-Z]{1})/g, g => `${g.substring(0, 4)}-${g[4].toLowerCase()}`)
        // handle words followed by dates
        .replace(/([A-Za-z]{1})([\d]{4})/g, '$1-$2')
        // handle molecules
        .replace(/([\d]{1}[A-Z]{1}?)([A-Z]{1})/g, (_g, ...args) => `${args[0]}-${args[1]}`.toLowerCase())
        // handle words followed by numbers
        .replace(/([A-Z][a-z]+)([0-9]{2,})/g, '$1-$2')
        // handle all capital letters
        .replace(/([A-Z])/g, g => `-${g[0].toLowerCase()}`)
        // handle underscores
        .replace(/[\_\.]/g, '-')
    : null;

/**
 * Returns current date in YYYY-MM-DD format.
 *
 * @returns Date as a string.
 */
export const now = () => new Date().toJSON().substring(0, 10);
