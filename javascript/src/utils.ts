export const isEmpty = (value: any, minKeys = 1) =>
  value === null ||
  typeof value === 'undefined' ||
  (typeof value === 'object'
    ? Array.isArray(value)
      ? !value.length
      : Object.keys(value).filter(key => key !== 'type').length < minKeys
    : value === '');

export const isIri = (value?: string) => (value || '').startsWith('http');

/* eslint-disable complexity */
export const isUndefined = <T>(value: T, allowNull = false) =>
  (value === null && !allowNull) ||
  typeof value === 'undefined' ||
  (typeof value === 'object' && value !== null && !(value instanceof Date) && !Object.keys(value).length);
/* eslint-enable complexity */

export const reduceUndefinedValues = <T>(obj: T, allowNull = false) =>
  Object.entries(obj).reduce((prev, [key, value]) => {
    return { ...prev, ...(isUndefined(value, allowNull) ? {} : { [key]: value }) };
  }, {} as Partial<T>);

export const filterUndefinedValues = <T>(values: T[], allowNull = false) =>
  values.filter(value => !isUndefined(value, allowNull));
