/* eslint-disable @typescript-eslint/no-loss-of-precision */
import { expect } from 'chai';
import 'mocha';

import { isNumber, toPrecision, convertValue, ConvertUnits, toComma } from './number';

describe('number', () => {
  describe('isNumber', () => {
    it('should return false for non numbers', () => {
      expect(isNumber('abc')).to.equal(false);
      expect(isNumber('abc123')).to.equal(false);
      expect(isNumber('false')).to.equal(false);
    });

    it('should return false for dates', () => {
      expect(isNumber('2020-01-02')).to.equal(false);
      expect(isNumber('2020-01')).to.equal(false);
      expect(isNumber(new Date().toJSON())).to.equal(false);
    });

    it('should return true for numbers', () => {
      expect(isNumber('0')).to.equal(true);
      expect(isNumber('1')).to.equal(true);
      expect(isNumber(123)).to.equal(true);
      expect(isNumber('123')).to.equal(true);
      expect(isNumber('5e-04')).to.equal(true);
      expect(isNumber('5e04')).to.equal(true);
      expect(isNumber('-35e04')).to.equal(true);
      expect(isNumber('1.2356')).to.equal(true);
      expect(isNumber('-1.2356')).to.equal(true);
      expect(isNumber('-1.23e-05')).to.equal(true);
    });
  });

  describe('toPrecision', () => {
    it('should handle zero', () => {
      expect(toPrecision(0)).to.equal(0);
    });

    it('should use significant figures', () => {
      expect(toPrecision(9.5)).to.equal(9.5);
      expect(toPrecision(9.501)).to.equal(9.5);
      expect(toPrecision(9.49)).to.equal(9.49);
      expect(toPrecision(9.499)).to.equal(9.5);
      expect(toPrecision(0.000152)).to.equal(0.000152);
      expect(toPrecision(3645.875, 3)).to.equal(3650);
      expect(toPrecision(2954618.3403312406, 3)).to.equal(2950000);
      expect(toPrecision(9089080.000000001111, 3)).to.equal(9090000);
      expect(toPrecision(14290011, 3)).to.equal(14290000);
    });
  });

  describe('toComma', () => {
    it('should set a comma every thousands', () => {
      expect(toComma(0)).to.equal('0');
      expect(toComma(0.000152)).to.equal('0.000152');
      expect(toComma(2954618.3403312406)).to.equal('2,954,618.3403312406');
    });

    it('should toPrecision with precision', () => {
      expect(toComma(toPrecision(9089080.000000001111, 3))).to.equal('9,090,000');
    });
  });

  describe('convertValue', () => {
    it('should convert units', () => {
      expect(convertValue(1, ConvertUnits.m3, ConvertUnits.kg, { density: 553 })).to.equal(553);
      expect(convertValue(1, ConvertUnits.m3, ConvertUnits.L)).to.equal(1000);
      expect(convertValue(553, ConvertUnits.kg, ConvertUnits.m3, { density: 553 })).to.equal(1);
      expect(convertValue(553, ConvertUnits.kg, ConvertUnits.L, { density: 553 })).to.equal(1000);
      expect(convertValue(1000, ConvertUnits.L, ConvertUnits.m3)).to.equal(1);
      expect(convertValue(1000, ConvertUnits.L, ConvertUnits.kg, { density: 553 })).to.equal(553);
      expect(Math.round(convertValue(100, ConvertUnits.MJ, ConvertUnits.kWh))).to.equal(28);
      expect(convertValue(10, ConvertUnits.kWh, ConvertUnits.MJ)).to.equal(36);

      // molar masses
      expect(convertValue(10, ConvertUnits.kgCH4, ConvertUnits.kgCH4C)).to.equal(13.356643356643357);
      expect(convertValue(10, ConvertUnits.kgCO2, ConvertUnits.kgCO2C)).to.equal(36.63836163836164);
      expect(convertValue(10, ConvertUnits.kgCa, ConvertUnits.kgCaO)).to.equal(7.146958646147262);
      expect(convertValue(10, ConvertUnits.kgCaO, ConvertUnits.kgCaCO3)).to.equal(5.602825541778653);
      expect(convertValue(10, ConvertUnits.kgCaCO3, ConvertUnits.kgCO2)).to.equal(1.2);
      expect(convertValue(10, ConvertUnits.kgMgCO3, ConvertUnits.kgCO2)).to.equal(1.3);
      expect(convertValue(10, ConvertUnits.kgCO2, ConvertUnits.kgCO2C)).to.equal(36.63836163836164);
      expect(convertValue(10, ConvertUnits.kgNOx, ConvertUnits.kgNOxN)).to.equal(21.422146069822233);
      expect(convertValue(10, ConvertUnits.kgN2, ConvertUnits.kgN2N)).to.equal(10);
      expect(convertValue(10, ConvertUnits.kgN2O, ConvertUnits.kgN2ON)).to.equal(15.711073034911117);
      expect(convertValue(10, ConvertUnits.kgNO2, ConvertUnits.kgNO2N)).to.equal(32.844292139644466);
      expect(convertValue(10, ConvertUnits.kgNO3, ConvertUnits.kgNO3N)).to.equal(44.26643820946669);
      expect(convertValue(10, ConvertUnits.kgNH3, ConvertUnits.kgNH3N)).to.equal(12.158920539730135);
      expect(convertValue(10, ConvertUnits.kgNH4, ConvertUnits.kgNH4N)).to.equal(12.87856071964018);
      expect(convertValue(10, ConvertUnits.kgP, ConvertUnits.kgP2O5)).to.equal(4.364287072979999);
      expect(convertValue(10, ConvertUnits.kgP, ConvertUnits.kgPO43)).to.equal(1.0871503281738095);
      expect(convertValue(10, ConvertUnits.kgPO43, ConvertUnits.kgP2O5)).to.equal(20.072141634317994);
      expect(convertValue(10, ConvertUnits.kgK, ConvertUnits.kgK2O)).to.equal(8.301502202877012);
      expect(convertValue(10, ConvertUnits.kgCaMg_CO3_2, ConvertUnits.kgCaCO3)).to.equal(10);
      expect(convertValue(10, ConvertUnits.kgCa_OH_2, ConvertUnits.kgCaCO3)).to.equal(10);
    });
  });
});
