import { expect } from 'chai';
import 'mocha';

import { filterParams } from './form';

describe('form', () => {
  describe('filterParams', () => {
    it('should filter empty params', () => {
      expect(
        filterParams({
          a: 1,
          b: null,
          c: {},
          d: [],
          e: 0,
          f: '',
          g: false,
          h: undefined,
          i: 'text'
        })
      ).to.deep.equal({
        a: '1',
        e: '0',
        g: 'false',
        i: 'text'
      });
    });
  });
});
