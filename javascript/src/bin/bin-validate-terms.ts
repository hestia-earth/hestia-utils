#!/usr/bin/env node
/* eslint-disable no-console */
import { run } from '../validate-terms';

run()
  .then(paths => {
    console.log('Done validating', paths.length, 'files.');
    console.log(paths.join('\n'));
    process.exit(0);
  })
  .catch(err => {
    console.log('Error validating files.');
    console.error(err);
    process.exit(1);
  });
