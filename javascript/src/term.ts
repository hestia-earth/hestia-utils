import { getArrayTreatment } from '@hestia-earth/glossary';

import { isEmpty, isUndefined } from './utils';
import { isNumber } from './number';
import { isBoolean } from './boolean';

export type propertyValueType = string | number | boolean | null;

export const arrayValue = (values: propertyValueType[] = [], isAverage = false) => {
  const filteredValues = values.filter(v => !isEmpty(v));

  return filteredValues.length === 0
    ? null
    : filteredValues.every(isNumber)
    ? (filteredValues as number[])
        .filter(v => typeof v !== 'undefined')
        .reduce((prev, curr) => prev + parseFloat(`${curr}`), 0) / (isAverage ? filteredValues.length : 1)
    : filteredValues.every(isBoolean)
    ? (filteredValues as boolean[]).every(Boolean)
    : filteredValues.join(';');
};

/**
 * Calculate the final value of a property.
 *
 * @param value The value as an array or a string/number/boolean.
 * @param termId Optional - us if the term should handle an array in a specific way.
 */
export const propertyValue = (value: propertyValueType | propertyValueType[], termId?: string) =>
  isUndefined(value)
    ? null
    : Array.isArray(value)
    ? arrayValue(value, (termId ? getArrayTreatment(termId) : null) === 'mean')
    : isNumber(value as number)
    ? parseFloat(`${value}`)
    : value;

/**
 * Checks if the value is empty or if the property value is empty.
 *
 * @param value
 * @returns
 */
export const emptyValue = (value: propertyValueType, termId?: string) =>
  isEmpty(value) || isNaN(propertyValue(value, termId) as number);
